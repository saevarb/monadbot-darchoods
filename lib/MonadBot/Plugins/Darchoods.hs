{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
module MonadBot.Plugins.Darchoods
    ( plugin
    ) where
import           Text.Printf
import           Data.Function
import           Data.Ord
import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import qualified Data.Text as T
import           System.Directory
import           System.IO
import           Data.Time
import           Data.Function (on)

import qualified MonadBot.DB as DB
import           MonadBot.Plugin.Development

darchoodsServer :: Text
darchoodsServer = "irc.darkscience.net"

dateFormat :: String
dateFormat = "%a %D"

channels :: [Text]
channels = ["#bots", "#420", "#monadbot"]

tokeIt :: [Text]
tokeIt =
 [ "                    ,"
 , "                   dM"
 , "                   MMr"
 , "                  4MMML                  ."
 , "                  MMMMM.                xf"
 , "  .              \"M6MMM               .MM-"
 , "   Mh..          +MM5MMM            .MMMM"
 , "   .MMM.         .MMMMML.          MMMMMh"
 , "    )MMMh.        MM5MMM         MMMMMMM"
 , "     3MMMMx.     'MMM3MMf      xnMMMMMM\""
 , "     '*MMMMM      MMMMMM.     nMMMMMMP\""
 , "       *MMMMMx    \"MMM5M\\    .MMMMMMM="
 , "        *MMMMMh   \"MMMMM\"   JMMMMMMP"
 , "          MMMMMM   GMMMM.  dMMMMMM            ."
 , "           MMMMMM  \"MMMM  .MMMMM(        .nnMP\""
 , "..          *MMMMx  MMM\"  dMMMM\"    .nnMMMMM*"
 , " \"MMn...     'MMMMr 'MM   MMM\"   .nMMMMMMM*\""
 , "  \"4MMMMnn..   *MMM  MM  MMP\"  .dMMMMMMM\"\""
 , "    ^MMMMMMMMx.  *ML \"M .M*  .MMMMMM**\""
 , "       *PMMMMMMhn. *x > M  .MMMM**\"\""
 , "          \"\"**MMMMhx/.h/ .=*\""
 , "                   .3P\"%...."
 , "        [nosig]  nP\"     \"*MMnx"
 ]

type User = Text

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
TokeEntry
    name User
    date UTCTime
    deriving Show
|]

initDb :: Irc ()
initDb = initializeDb (runMigration migrateAll)

setBotMode :: SimpleHandler
setBotMode = onCmd "376" $
    onlyForServer darchoodsServer $ do
        nick' <- myNick <$> getGlobalEnv
        sendCommand "MODE" [nick', "+B"]

toke :: SimpleHandler
toke = onUserCmd "$toke" $ do
    pref <- getPrefix
    (chan:_) <- getParams
    case pref of
      Just (UserPrefix p _ _) -> do
          time <- liftIO getCurrentTime
          runDb $ DB.insert $ TokeEntry p time
          res <- runDb $ DB.selectList [TokeEntryName DB.==. p] []
          sendPrivmsg chan ["Toke accepted."]
          sendPrivmsg chan ["You have toked", T.pack . show $ length res, "times."]
          return ()

stats :: SimpleHandler
stats = onUserCmd "$stats" $ do
    params <- getParams
    (Just pref) <- getPrefix
    let (chan, name) = case params of
               (ch:_:na:_) ->
                   (ch, na)
               (ch:_) ->
                   (ch, pNick pref)
    res' <- map DB.entityVal <$> runDb (DB.selectList [TokeEntryName DB.==. name] [])
    case res' of
        [] -> do
            sendPrivmsg chan ["No no no.", name, "no smoke."]
        res -> do
            let highestDay = maximumBy (comparing length)
                            $ groupBy ((==) `on` snd)
                            $ sortBy (comparing snd) days
                days = map (\d -> (d, utctDay d)) dates
                perDay = fromIntegral count /
                        fromIntegral (length . nub . map utctDay $ dates)
                count = length res
                dates = map tokeEntryDate res
            sendPrivmsg chan [ "Stats for", name ]
            sendPrivmsg chan [ "Times smoked:", T.pack $ show count ]
            sendPrivmsg chan [ "Avg. tokes per day on days you toked:"
                            -- , T.pack $ (printf "%.2f" perDay :: String)
                            , T.pack $ printf "%.2f" (perDay :: Double)
                            ]
            sendPrivmsg chan [ "Highest day:"
                            , T.pack $ formatTime defaultTimeLocale dateFormat
                                    (fst . head $ highestDay)
                            , "(" <> (T.pack . show $ length highestDay) <> " times)"
                            ]

top :: SimpleHandler
top = onUserCmd "$top" $ do
    pref <- getPrefix
    (chan:_) <- getParams
    case pref of
      Just (UserPrefix p _ _) -> do
        all <- runDb $ DB.selectList [] [DB.Asc TokeEntryName]
        let top10 = map (\es -> (name . head $ es, length es))
                    $ take 5
                    $ sortBy (flip (comparing length))
                    $ groupBy ((==) `on` name) all
        sendPrivmsg chan ["Top 5 tokers"]
        forM_ top10 $ \(n, c) ->
            sendPrivmsg chan [n, "@", T.pack $ show c, "tokes"]
  where
    name = tokeEntryName . entityVal

plugin :: Plugin ()
plugin = Plugin "Darchoods plugin" handlers initDb (const $ return ())
  where
    -- handlers :: [PluginM () ()]
    handlers =
        [ setBotMode
        , stats
        , toke
        , top
        ]
